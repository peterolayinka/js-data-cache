# Improve rendering performance through caching

## 1. Startegies to evict data from cache

Here are the possible strategies I can think of:

* Each cached data have a TTL (time to live)

* Purge data with low usage frequency

* Hard limit on cachable data

---

### Each cached data have a TTL (time to live)

This helps to ensure that data is not store forever; hence we are sure that we do not store obsolete cached data and the entry is cleanup after X duration.

### Purge data with low usage frequency

This helps to ensure that low usage frequency data isn't occupying the system's memory and we could use this method to identify cached data that is being used more frequently and possible store it for a longer period of time. basically removing the data based on usage.

### Hard Limit on cacheable data

We could use this to determine that after x number of entry; remove old entries

## 2. Preferred stragey

My most preferred strategy is to assign a TTL to each entry because this helps to ensure that this entry will only be available for X duration, but to improve this strategy for better optimization result. All 3 strategies I listed above can be combined. because determine usage of the cache will help me to retain the most used cache instead of deleting it and recreating again while the combination of hard limit help me stay in check in case I have more frequently accessed cache, this means the cache list can keep growing indefinately, but the hard limit can help me cut down the growth.

## 3. Data Strucure used

The data strature would be something like

``` js
data = {
  id: string,
  word: string, // the specific word that we are getting the rendered dimensions for
  font: string, // a font string, like '30px Arial'
  textMetric: TextMetrics(), // instace of TextMetric stored to cache,
  lastFetched: timestamp,
  created: timestamp,
}
```

If the strategies are combined we could use strategy 2 to retain cached data with more frequent usage hence we wouldn't have to re-create the same cache entry after deleting the entry.

While strategy 3 can help us to ensure we aren't exceeding X number of entries regardless of the other strategy used for better memory management.

Please see demo.js for the implmentation

## 4. Test

The following are scenerios I would test for in the evictEntries method to ensure the method works as expected

* X number of entries are evicted when the expired cache has more than X entries

* Cache remains the same when cache data is less than the number of entries to evict

* All eligible expired entry is deleted when 0 number of entry is passed to be evicted

* No entry is deleted when 0 number of entry is passed to be evicted and there are no eligible entry to be evicted

* ensure evicted entries are the oldest from the eligible entries to be evicted

* Ensure entries recently fetcged is exempted from cleanup

* Ensure data can be hard deleted

Run the sample test cases with command `yarn test`

**NOTE:** Please run `yarn install` before running the test
