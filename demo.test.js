import { measureText, TextMetricsCache, oneHour } from "./demo";

function loadTextMetrics(dataCount ,textMetricsCache) {
  for (let i = 0; i < dataCount; i++) {
    const word = `word_${i}`;
    const font = `${i+1}0px arial`;
    measureText(word, font, textMetricsCache);
  }
}

describe('cached data evicted', () => {
  let textMetricsCache;

  beforeEach(() => {
    textMetricsCache = new TextMetricsCache();
    return loadTextMetrics(10, textMetricsCache);
  });

  test('when number of entries to evict are eligible to be evicted', () => {
    // force 2 entries to be evicted
    const expiredDuration = oneHour*26;
    textMetricsCache.cacheData[0].created -= expiredDuration;
    textMetricsCache.cacheData[9].created -= expiredDuration;

    expect(textMetricsCache.cacheData.length).toBe(10);
    const evictedEntryCount = textMetricsCache.evictEntries(2);
    expect(evictedEntryCount).toBe("2 entries evicted");
    expect(textMetricsCache.cacheData.length).toBe(8);
  });
  
  test('when an extry meant to be evicted has been recently fetched', () => {
    // force 2 entries to be evicted
    const expiredDuration = oneHour*26;
    textMetricsCache.cacheData[0].created -= expiredDuration;
    textMetricsCache.cacheData[0].lastFetched = Date.now();
    textMetricsCache.cacheData[9].created -= expiredDuration;

    expect(textMetricsCache.cacheData.length).toBe(10);
    const evictedEntryCount = textMetricsCache.evictEntries(2);
    expect(evictedEntryCount).toBe("1 entries evicted");
    expect(textMetricsCache.cacheData.length).toBe(9);
  });
  
  test('when hard deleted', () => {
    expect(textMetricsCache.cacheData.length).toBe(10);
    const evictedEntryCount = textMetricsCache.evictEntries(5, true);
    expect(evictedEntryCount).toBe("5 entries evicted");
    expect(textMetricsCache.cacheData.length).toBe(5);
  });

});