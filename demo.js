export const oneHour = 1000 * 60 * 60;
const twentyFourHour = oneHour * 24;

export class TextMetrics {
  constructor(word, font, context) {
    context.font = font;
    context.fillText(word, 0, 0);

    // These methods are expensive to compute:
    this.width = this._calculateTextWidth(context);
    this.ascenderHeight = this._calculateAscenderHeight(context);
    this.descenderHeight = this._calculateDescenberHeight(context);
  }

  // method placeholder for TextMetrics.witdh
  async _calculateTextWidth(context) {}
  // method placeholder for TextMetrics.ascenderHeight
  async _calculateAscenderHeight(context) {}
  // method placeholder for TextMetrics.descenderHeight
  async _calculateDescenberHeight(context) {}
}

export class TextMetricsCache {
  /**
   * This method handles metric cache logic
   */
  constructor(ttl = twentyFourHour, hardLimit = 1000, usageFreqTime = oneHour) {
    // The data structure is as follows:
    // {
    //   word: string, // the specific word that we are getting the rendered dimensions for
    //   font: string, // a font string, like '30px Arial'
    //   textMetric: TextMetrics(), // instace of TextMetric stored to cache,
    //   lastFetched: timestamp,
    //   created: timestamp,
    // }
    // your data structure is stored in this.cacheData
    this.cacheData = [];

    // if you need any other class variables,
    // you can add them here:

    this.ttl = ttl;
    this.usageFreqTime = usageFreqTime;
    this.hardLimit = hardLimit;
  }

  getCachedEntry(word, font) {
    // please write the code to look up and return the stored
    // TextMetrics, if it exists in the cache, for the input
    // word and font strings.
    const validCacheIndex = this.cacheData.findIndex(
      (element) => element.word === word && element.font === font
    );
    if (validCacheIndex === -1) {
      return null;
    }
    const cachedEntry = this.cacheData[validCacheIndex]
    cachedEntry.lastFetched = Date.now();
    return cachedEntry;
  }

  setCachedEntry(word, font, textMetricsData) {
    // please write the code to store the textMetricsData
    // which corresponds to the  input word and font strings

    // strategy 3 implementation
    if (this.hardLimit > 0 && this.cacheData.length >= this.hardLimit) {
      const numEntriesToEvict = this.cacheData.length - this.hardLimit;
      this.evictEntries(numEntriesToEvict, true);
    }

    const id = Math.random().toString(36).substr(2, 9);
    this.cacheData.push({
      id,
      word: word,
      font: font,
      textMetric: textMetricsData,
      lastFetched: null,
      created: Date.now(),
    });
  }

  /**
   * This method will be called when there are too many entries
   * in the cache and we need to evict some of them.
   *
   * @param {integer} numEntriesToEvict - the number of entries that need
   *                                      to be evicted from this.cacheData
   */
  evictEntries(numEntriesToEvict, hard=false) {
    // Please write the code to choose entries to evict and evict them,
    // according to the strategy/data structure you chose in Question 2.
    let evictedEntryCount = 0;
    const expiredDuration = Date.now() - this.ttl;
    const usageDuration = Date.now() - this.usageFreqTime;

    if (!hard) {
      const expiredCache = this.cacheData.filter(
        (cache) => cache.created < expiredDuration
      );
      if (expiredCache.length >= numEntriesToEvict) {
        const sortedCachedData = expiredCache.sort(
          (a, b) => a.created - b.created
        );

        if (numEntriesToEvict == 0) {
          numEntriesToEvict = expiredCache.length;
        }

        for (let i = 0; i < numEntriesToEvict; i++) {
          // check entry usage and evict if necessary
          const entryToEvict = sortedCachedData[i];
          if (entryToEvict.lastFetched >= usageDuration) {
            continue;
          }

          // remove the entry from the cache
          this.cacheData.splice(this.cacheData.indexOf(entryToEvict), 1);
          evictedEntryCount++;
        }
      }
    } else {
      this.cacheData.splice(0, numEntriesToEvict);
      evictedEntryCount = numEntriesToEvict;
    }

    return `${evictedEntryCount} entries evicted`;
  }
}

/**
 * Measure the dimensions of a word, when it is rendered with a specific font
 *
 * @param {string} word - the specific word that we are
 *                        getting the rendered dimensions for
 * @param {string} font - a font string, like '30px Arial'. See
 *                        https://www.w3schools.com/tags/canvas_font.asp
 * @returns {TextMetrics} the measured metrics object
 */
export function measureText(word, font, cache = null) {
  // We would like to cache and re-use instances of TextMetrics when we call measureText
  if (cache === null) {
    // pull cache from global cache, 
    // NOTE: this is a placeholder for your code
    cache = new TextMetricsCache();
  }

  let cachedEntry = cache.getCachedEntry(word, font);
  if (cachedEntry !== null) {
    return cachedEntry.textMetric;
  }

  const context = document.createElement("canvas").getContext("2d");
  const textMetric = new TextMetrics(word, font, context);
  cache.setCachedEntry(word, font, textMetric);
  return textMetric;
}
